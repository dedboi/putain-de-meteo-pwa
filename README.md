# Basic weather app

## Using
    * Nuxt with PWA Module
    * Axios for Requests
    * MetaWeatherAPI for weather data
    * Vuetify UI components

Inspired by [Snacklabs tutorial](http://snacklabs.nativescriptsnacks.com/snacklabs/vue-weather/#1)

Thanks to [@jefrydco](https://github.com/jefrydco) for his [Jefry Dewangga](https://github.com/jefrydco/nuxt-pwa-vuetify-starter) for the starter template of nuxt pwa with vuetify & other modules and cool stuff
require('dotenv').config()
const nodeExternals = require('webpack-node-externals')
const resolve = (dir) => require('path').join(__dirname, dir)

module.exports = {

  /* Manifest */
  manifest: {
    name: 'putain de météo',
    lang: 'fr'
  },
  /*
  ** Headers of the page
  */
  head: {
    title: 'Putain de météo !',
    meta: [
      {
        charset: 'utf-8'
      }, {
        name: 'viewport',
        content: 'width=device-width, initial-scale=1'
      }, {
        hid: 'description',
        name: 'description',
        content: 'Progressive Web app using nuxt vuetify firebase axios fontawesome'
      }
    ],
    link: [
      {
        rel: 'icon',
        type: 'image/x-icon',
        href: '/favicon.ico'
      }, {
        rel: 'stylesheet',
        href: 'https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Material+Icons'
      }
    ]
  },

  /**
   * Plugins
   */
  plugins: [
    '~/plugins/vuetify.js', '~/plugins/components.js'
  ],

  /**
   * Modules
   */
  modules: [
    '@nuxtjs/axios',
    '@nuxtjs/browserconfig',
    '@nuxtjs/component-cache',
    '@nuxtjs/dotenv',
    '@nuxtjs/pwa',
    '@nuxtjs/sentry',
    '@nuxtjs/sitemap',
    'nuxt-fontawesome'
  ],

  /*
   * FontAwesome
   */
  fontawesome: {
    component: 'fa',
    imports: [
      {
        set: '@fortawesome/free-solid-svg-icons',
        icons: ['fas']
      }
    ]
  },

  /**
   * Custom CSS
   */
  css: [
    '~/assets/style/vuetify.styl',
    '~/assets/style/main.css'
  ],

  /*
  ** Customize the progress bar color
  */
  loading: {
    color: '#82b1ff',
    failedColor: '#ff8a80',
    height: '5px'
  },

  axios: {
    proxyHeaders: false,
    credentials: false
  },

  /*
  ** Build configuration
  */
  build: {

    /**
     * Babel configutation
     */
    babel: {
      plugins: [
        'transform-runtime',
        [
          'transform-imports', {
            'vuetify': {
              'transform': 'vuetify/es5/components/${member}',
              'preventFullImport': true
            }
          }
        ]
      ]
    },

    /**
     * Vendor
     */
    vendor: ['~/plugins/vuetify.js'],

    /**
     * Extract CSS
     */
    extractCSS: true,

    /*
    ** Run ESLint on save
    */
    extend(config, {isDev, isClient, isServer}) {
      if (isDev && isClient) {
        config
          .module
          .rules
          .push({enforce: 'pre', test: /\.(js|vue)$/, loader: 'eslint-loader', exclude: /(node_modules)/})
      }
      if (isServer) {
        config.externals = [nodeExternals({whitelist: [/^vuetify/]})]
      }
    }
  }
}
